import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}

  // 1. Get Api Data
  apiData() {
    return this.http.get('https://reqres.in/api/users?page=2');
  }

  // 2. Delete Api Data
  deleteapiData(id: number) {
    return this.http.delete(`https://reqres.in/api/users/${id}`);
  }

  // 3. Post Api Data
  postapiData(userData: any) {
    return this.http.post(`https://reqres.in/api/users`, userData);
  }

  // 4. Update Api Data
  updateapiData(updatedData: any) {
    const id = updatedData.id;
    return this.http.put(`https://reqres.in/api/users/${id}`, updatedData);
  }
}
