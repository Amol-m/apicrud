import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent {
  public data1: any = [];
  public stdData: FormGroup;
  public formVisible: boolean = false;
  public isUpdate: boolean = false;
  private currentUserId: number | null = null;

  constructor(private service: DataService, private fb: FormBuilder) {
    this.stdData = this.fb.group({
      id: [''],
      email: [''],
      first_name: [''],
      last_name: [''],
      avatar: [''],
    });
  }

  ngOnInit() {
    this.ApiData1();
  }

  // 1. Get Api Data
  ApiData1() {
    this.service.apiData().subscribe({
      next: (res: any) => {
        this.data1 = res.data;
        console.log(this.data1);
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  // 2. Delete Api Data
  deleteApiData(id: number) {
    this.service.deleteapiData(id).subscribe({
      next: (res: any) => {
        const index = this.data1.findIndex((user: any) => user.id === id);
        if (index !== -1) {
          this.data1.splice(index, 1);
        }
        console.log(`User with id ${id} deleted`);
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  // Submit Data
  onSubmitData() {
    if (this.isUpdate && this.currentUserId !== null) {
      this.updateData();
    } else {
      this.service.postapiData(this.stdData.value).subscribe({
        next: (res: any) => {
          console.log('Post response:', res);
          this.data1.push(res);
          this.stdData.reset();
          this.formVisible = false;
        },
        error: (err: any) => {
          console.log(err);
        },
      });
    }
  }

  // Toggle Form
  toggleForm() {
    this.formVisible = !this.formVisible;
    this.isUpdate = false;
    this.stdData.reset();
  }

  // Populate Form with Existing Data
  updateData1(data: any) {
    this.formVisible = true;
    this.isUpdate = true;
    this.currentUserId = data.id;
    this.stdData.setValue({
      id: data.id,
      email: data.email,
      first_name: data.first_name,
      last_name: data.last_name,
      avatar: data.avatar,
    });
  }

  // Update Data
  updateData() {
    const updatedData = this.stdData.value;
    this.service.updateapiData(updatedData).subscribe({
      next: (res: any) => {
        console.log('Update response:', res);
        const index = this.data1.findIndex(
          (user: any) => user.id === updatedData.id
        );
        if (index !== -1) {
          this.data1[index] = updatedData;
        }
        this.stdData.reset();
        this.formVisible = false;
        this.isUpdate = false;
        this.currentUserId = null;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }
}
